//
//  ViewController.swift
//  helloworld
//
//  Created by user on 17.02.2019.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func showAlert(){
        let alert = UIAlertController(
            title: "Hello World",
            message: "this is my first app",
            preferredStyle: .alert)
     
        let action = UIAlertAction(title: "Awesome", style: .default, handler: nil)
        
        alert.addAction(action);
        present(alert, animated: true, completion: nil);
    }
}

